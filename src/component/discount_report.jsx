import React, { useState, useEffect, useRef } from 'react';
import { AgGridColumn, AgGridReact } from 'ag-grid-react';
import moment from "moment";
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import axios from 'axios';
// import Backdrop from '@material-ui/core/Backdrop';
// import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';



const useStyles = makeStyles((theme) => ({
    root: {
        // flexGrow: 1,



    },
    head: {
        display: 'flex',
        textAlign: 'center',
        alignItems: 'center',
        width: '100%',
        justifyContent: 'center',
        columnGap: '20px'

    },
    main: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',

    },
    "@media (min-width: 300px ) and (max-width:1100px)": {
        main: {
            display: 'block',
        },
        btns:{
            outline: "unset",
        },
        btn: {
          
            margin:'10px auto',
    
        }
    },
    btn: {
        display: 'flex',
       
    },
    

    dt1:{
        marginRight:'10px',
    
    },
    


    // page: {
    //     '& > *': {
    //         marginTop: theme.spacing(2),
    //     },
    // },

    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
    },
    downloadbutton: {
        display: "block",
    }
}));


const Discount_Report = () => {
    const classes = useStyles();
    const [rowData, setRowData] = useState([]);
    const [number, setNumber] = useState(1000);
    const [startDate, setStartDate] = useState(new Date().getFullYear() + '-' + ("0" + (new Date().getMonth() + 1)).slice(-2) + '-' + ("0" + new Date().getDate()).slice(-2));
    const [endDate, setEnddate] = useState(new Date().getFullYear() + '-' + ("0" + (new Date().getMonth() + 1)).slice(-2) + '-' + ("0" + new Date().getDate()).slice(-2))
    const [page, setPage] = useState(1);
    const [fullData, setFullData] = useState([]);
    const [order, setOrder] = useState([]);
    const [allOrderData, setallOrderData] = useState([]);
    let [finalOrderData, setfinalOrderData] = useState([]);
    const [isPageLoaded, setisPageLoaded] = useState(false);
    const [isStart, setisStart] = useState(false);
    const [isProductLoaded, setisProductLoaded] = useState(false);
    let [rowDataL, setrowDataL] = useState([]);
    let [rowDataL1, setrowDataL1] = useState([]);

    const states = [
        {
            index: 1,
            stateCode: 'AP',
            stateName: 'Andhra Pradesh',
        },
        {
            index: 2,
            stateCode: 'AR',
            stateName: 'Arunachal Pradesh	'
        },
        {
            index: 3,
            stateCode: 'AS',
            stateName: 'Assam'
        },
        {
            index: 4,
            stateCode: 'BR',
            stateName: 'Bihar'
        },
        {
            index: 5,
            stateCode: 'CG',
            stateName: 'Chhattisgarh'
        },
        {
            index: 6,
            stateCode: 'GA',
            stateName: 'Goa',
        },
        {
            index: 7,
            stateCode: 'GJ',
            stateName: 'Gujarat',
        },
        {
            index: 8,
            stateCode: 'HR',
            stateName: 'Haryana',
        },
        {
            index: 9,
            stateCode: 'HP',
            stateName: 'Himachal Pradesh	',
        },
        {
            index: 10,
            stateCode: 'JH',
            stateName: 'Jharkhand',
        },
        {
            index: 11,
            stateCode: 'KA',
            stateName: 'Karnataka',
        },
        {
            index: 12,
            stateCode: 'KL',
            stateName: 'Kerala	',
        },
        {
            index: 13,
            stateCode: 'MP',
            stateName: 'Madhya Pradesh',
        },
        {
            index: 14,
            stateCode: 'MH',
            stateName: 'Maharashtra',
        },
        {
            index: 15,
            stateCode: 'MN',
            stateName: 'Manipur',
        },
        {
            index: 16,
            stateCode: 'ML',
            stateName: 'Meghalaya',
        },
        {
            index: 17,
            stateCode: 'MZ',
            stateName: 'Mizoram',
        },
        {
            index: 18,
            stateCode: 'NL',
            stateName: 'Nagaland',
        },
        {
            index: 19,
            stateCode: 'OR',
            stateName: 'Odisha',
        },
        {
            index: 20,
            stateCode: 'PB',
            stateName: 'Punjab',
        },
        {
            index: 21,
            stateCode: 'RJ',
            stateName: 'Rajasthan',
        },
        {
            index: 22,
            stateCode: 'SK',
            stateName: 'Sikkim',
        },
        {
            index: 23,
            stateCode: 'TN',
            stateName: 'Tamil Nadu	',
        },
        {
            index: 24,
            stateCode: 'TS',
            stateName: 'Telangana',
        },
        {
            index: 25,
            stateCode: 'TR',
            stateName: 'Tripura	',
        },
        {
            index: 26,
            stateCode: 'UP',
            stateName: 'Uttar Pradesh',
        },
        {
            index: 27,
            stateCode: 'UK',
            stateName: 'Uttarakhand',
        },
        {
            index: 28,
            stateCode: 'WB',
            stateName: 'West Bengal',
        },
        {
            index: 29,
            stateCode: 'CA',
            stateName: 'Canada',
        },
        {
            index: 30,
            stateCode: '',
            stateName: '-',
        },
        {
            index: 31,
            stateCode: 'DL',
            stateName: 'Delhi',
        },
        {
            index: 32,
            stateCode: 'JK',
            stateName: 'Jammu and Kashmīr'
        },
        {
            index: 33,
            stateCode: 'AN',
            stateName: 'Andaman and Nicobar Islands'
        },
        {
            index: 34,
            stateCode: 'CH',
            stateName: 'Chandīgarh	'
        },
        {
            index: 35,
            stateCode: 'DH',
            stateName: 'Dādra and Nagar Haveli and Damān and Diu	'
        },
        {
            index: 36,
            stateCode: 'LA',
            stateName: 'Ladākh	'
        },
        {
            index: 37,
            stateCode: 'LD',
            stateName: 'Lakshadweep	'
        },
        {
            index: 38,
            stateCode: 'PY',
            stateName: 'Puducherry		'
        },
        {
            index: 39,
            stateCode: 'CT',
            stateName: 'Chhattisgarh		'
        },

    ]




    useEffect(() => {
        const headers = {
            'Content-Type': 'application/json',
            Authorization: 'Basic Y2tfYmVkZDVkOGQ3MjczMGEyNWM2NDk4NGJjY2JlOTZhOGZiOGUwZDhmNzpjc181MjVlMmU5MDY3YWVmYThjN2U2MDViZTIwMTY5Nzc4OTdlODM4ZTVj',
        }
        let orders = []
        axios.get(`https://mysparklebox.com/wp-json/wc/v2/orders/?after=` + startDate + `T00:00:00&before=` + endDate + `T23:59:59&per_page=${100}&page=${page}`, { headers })
            .then(data => {
                // console.log(data.headers);
                if (data?.data?.length > 0) {
                    setRowData(data?.data);
                }
            }
            );
    }, [page, startDate, endDate]);

    function getData(loop) {
        //setisPageLoaded(false);
        //for(var i=1;i<=loop;i++){
        const headers = {
            'Content-Type': 'application/json',
            Authorization: 'Basic Y2tfYmVkZDVkOGQ3MjczMGEyNWM2NDk4NGJjY2JlOTZhOGZiOGUwZDhmNzpjc181MjVlMmU5MDY3YWVmYThjN2U2MDViZTIwMTY5Nzc4OTdlODM4ZTVj',
        }

        axios.get(`https://mysparklebox.com/wp-json/wc/v2/orders/?after=` + startDate + `T00:00:00&before=` + endDate + `T23:59:59&per_page=${100}&page=${loop}`, { headers })
            // .then(response => response.json())
            .then(data => {
                console.log(data.data);
                allOrderData.push(data.data);

                if (loop == 1) {
                    setisPageLoaded(true);
                    setisStart(false);
                    setisProductLoaded(false);
                    var newArrayOrder = allOrderData.flat(Infinity);
                    console.log(newArrayOrder);
                    //allOrderData=[...newArrayOrder];
                    // getProduct();
                    setallOrderData(allOrderData.flat(Infinity));
                    finalOrderData = allOrderData.flat(Infinity);
                    setfinalOrderData(finalOrderData);
                    //setRowData(newArrayOrder);
                    //console.log("ROW DATA");
                    console.log(finalOrderData);
                }
                else {
                    getData(loop - 1);
                }
            }
            );
        // }
    }

    useEffect(() => {
        if (isPageLoaded) {
            console.log(allOrderData);
        }
    }, [isPageLoaded, allOrderData]);
    let product_id;
    console.log('rowData', rowData)




    let productId, prod_details, productName, quantity, price, total_tax, discount, code, skuCode, state_name, rate_percent, transaction_id, date_paid, skuName, unitsSold, DeadWeight, Volumetric, Channel, LineItems, orderDate, dheight, dwidth, dlength, dweight;
    let sNo;
    useEffect(() => {
        if (finalOrderData.length > 0) {
            // console.log('rowdata;', rowData);
            // console.log('products:', product);
            setRowData([])
            setrowDataL([])
            setrowDataL1([])

            finalOrderData?.map((n, i) => {
                // console.log('n', i)
                unitsSold = n.line_items

                // console.log(unitsSold)
                unitsSold.map((prod, index) => {
                    // console.log(prod.name, prod.id, n.id)
                    prod_details = prod.name
                    productName = prod.name;
                    quantity = prod.quantity;
                    productId = prod.product_id;
                    skuCode = prod?.sku;
                    price = parseInt(prod.price);
                    total_tax = prod.total_tax;

                    let state_name = ''
                    states.map(state => {
                        if (state.stateCode === n.billing?.state)
                            state_name = state.stateName
                    });
                    let disc = 0;
                    let gst_total;
                    n.fee_lines?.map((fee) => {
                        disc = parseFloat(disc) + parseFloat(fee.amount);
                        fee.taxes?.map((ft) => {
                            gst_total = ft.total
                        })
                    });

                    n.coupon_lines?.map((c) => {
                        discount = c.discount;
                        code = c.code;
                    })
                    n.tax_lines?.map((t) => {
                        rate_percent = t.rate_percent;

                    })
                    let date = moment(n.date_created).format("L");
                    let date_paid = moment(n.date_paid).format("L");
                    let total = 0;
                    let partialTotal = 0;
                    let payment_method = "";
                    let payment_method1 = "";
                    let payment_method2 = "";
                    //   let dispatchDate = "";
                    //   let skuName = '';
                    n?.meta_data?.map((partial) => {
                        if (partial?.key === "mwb_woo_pcod_saved_partial_amount") {
                            partialTotal = parseFloat(n?.total) - 99;
                            payment_method = "Partial COD";
                        }
                        if (n?.payment_method_title === "Pay with Razorpay" || n?.payment_method_title === "Sezzle" || n?.payment_method_title === "Zest") {
                            payment_method2 = "Pre-Paid";
                            total = parseFloat(n?.total);
                        }
                        
                        else {
                            payment_method1 = 'COD';
                            total = parseFloat(n?.total);
                        }
                    })

                    if (page > 1) {
                        sNo = parseInt((number * (page - 1)) + (i + 1));
                    } else {
                        sNo = parseInt(i + 1);
                    }

                    if (n.status != 'cancel-request'
                        && n.status != 'cancelled'
                        && n.status != 'failed'
                    ) {
                        rowDataL.push({
                            // 'sno': sNo,
                            'date_created': date,
                            'id': n.id,
                            'status': n.status,
                            'state': n.state,
                            'state_name': state_name,
                            // 'lineitems': productId,
                            'ProductName': productName,
                            'quantity': quantity,
                            'quntity': n.quntity,
                            'price': price,
                            'total_tax': total_tax,
                            'rate_percent': rate_percent,
                            'discount': discount,
                            'code': code,
                            'disc': disc,
                            'gst_total': gst_total,
                            // 'Dead Weight': productName,
                            'billing': n.billing,
                            'shipping': n.shipping,
                            'payment_method_title': payment_method ? payment_method : payment_method2 ? payment_method2 : payment_method1,
                            'transaction_id': n.transaction_id,
                            'date_paid': date_paid,
                            'total': partialTotal ? partialTotal : total,

                        })

                    }
                    //     }
                    //   })
                })
            })
            setisProductLoaded(false);
            setrowDataL1(rowDataL);

        }

    }, [finalOrderData, rowDataL, rowDataL1])



    const [gridApi, setGridApi] = useState([]);

    const colDef = [
        // { headerName: 'S.No', field: 'sno', showRowGroup: true, minWidth: 120, },
        {
            headerName: 'Order Date', field: 'date_created', showRowGroup: true, minWidth: 220,
        },
        { headerName: 'Order ID', field: 'id', showRowGroup: true, minWidth: 220, },
        { headerName: 'State Code', field: 'billing.state', showRowGroup: true, minWidth: 220, },
        { headerName: 'State Name', field: 'state_name', showRowGroup: true, minWidth: 220, },
        // { headerName: 'Product ID', field: 'lineitems', showRowGroup: true, minWidth: 220, cellRenderer: 'agGroupCellRenderer', },
        { headerName: 'Units Sold', field: 'quantity', showRowGroup: true, minWidth: 220, },
        { headerName: 'Product Name', field: 'ProductName', showRowGroup: true, minWidth: 320, },
        { headerName: 'Status', field: 'status', showRowGroup: true, minWidth: 220, },
        { headerName: 'Item Price', field: 'price', showRowGroup: true, minWidth: 220, },
        { headerName: 'Item Tax', field: 'total_tax', showRowGroup: true, minWidth: 220, },
        { headerName: 'Item Tax Rate', field: 'rate_percent', showRowGroup: true, minWidth: 220, },
        { headerName: ' Coupon Discount', field: 'discount', showRowGroup: true, minWidth: 220, },
        { headerName: ' Coupon Code', field: 'code', showRowGroup: true, minWidth: 220, },
        { headerName: 'Additional Discount', field: 'disc', showRowGroup: true, minWidth: 220, },
        { headerName: 'GST', field: 'gst_total', showRowGroup: true, minWidth: 220, },
        { headerName: 'Customer Name', field: 'billing.first_name', showRowGroup: true, minWidth: 220, },
        { headerName: 'Payment Id', field: 'transaction_id', showRowGroup: true, minWidth: 220, },
        { headerName: 'Payment Date', field: 'date_paid', showRowGroup: true, minWidth: 220, },
        { headerName: 'Payment Gateway', field: 'payment_method_title', showRowGroup: true, minWidth: 220, },
        { headerName: 'Total', field: 'total', showRowGroup: true, minWidth: 220, },
    ]
    const defaultColDef = {
        flex: 1,
        minWidth: 150,
        filter: true,

        resizable: true,
    };



    const onGridReady = (params) => {

        setGridApi(params);
    };
    const onBtnExport = () => {

        gridApi.api.exportDataAsCsv();

    };

    const onGenReport = () => {
        setisStart(true);
        setisProductLoaded(true);
        setisPageLoaded(false);
        const headers = {
            'Content-Type': 'application/json',
            Authorization: 'Basic Y2tfYmVkZDVkOGQ3MjczMGEyNWM2NDk4NGJjY2JlOTZhOGZiOGUwZDhmNzpjc181MjVlMmU5MDY3YWVmYThjN2U2MDViZTIwMTY5Nzc4OTdlODM4ZTVj',
        }
        let orders = []
        axios.get(`https://mysparklebox.com/wp-json/wc/v2/orders/?after=` + startDate + `T00:00:00&before=` + endDate + `T23:59:59&per_page=${100}&page=${page}`, { headers })
            // .then(response => response.json())
            .then(data => {
                console.log(data.headers);
                allOrderData.length = 0;
                getData(parseInt(data.headers['x-wp-totalpages']));
                if (data?.data?.length > 0) {
                    setRowData(data?.data);
                }
            }
            );
    };

    // function handlePrev() {
    //     if (page > 1) {
    //         setPage(page - 1);
    //     }
    // }
    // function handleNext() {
    //     setPage(page + 1);
    // }
    console.log('fullData', fullData);
    console.log('order', order);
    console.log('rowdata', rowDataL);

    return (
        <div className={classes.root}>
            <Grid spacing={2} className={classes.head}>
                <Grid container item xs={12} md={12} lg={12}>
                    <Grid item xs={12} md={12} lg={12} className={classes.main}>
                        <div >{isProductLoaded ? <p className='dt1' style={{marginRight:'10px',padding: '10px', background: '#f55e5e', color: 'white', borderRadius: '10px' }}>Loading Orders Please wait...</p>: ''}
                        </div>
                        <br />
                        <div className='dt1'> <label style={{  marginRight:'10px',fontSize: 'large' }}>Start Date:</label>
                            <input
                                style={{ marginRight:'10px',borderRadius: '30px', padding: '10px', color: 'darkblue' }}
                                type='date'
                                value={startDate}
                                onChange={(e) => setStartDate(e.target.value)}
                            />
                        </div>
                    <div className='dt1'>
                    <label style={{marginRight:'10px', fontSize: 'large' }}>End Date:</label>
                        <input style={{marginRight:'10px', borderRadius: '30px', padding: '10px', color: 'darkblue' }}
                            type='date'
                            value={endDate}
                            onChange={(e) => setEnddate(e.target.value)}
                        />
                    </div>   
                        <div className='btn'>
                            <button className='btns' style={{
                                marginRight:'10px',
                                backgroundColor: '#0d0646',
                                padding: '13px',
                                borderColor: 'darkblue',
                                color: '#e0e0f5',
                                // fontSize: 'initial',
                                fontWeight: '700',
                                borderRadius: '30px'
                                // float:'right'

                            }}
                                onClick={() => onGenReport()}>
                                {isStart == false && isPageLoaded == false ? 'Generate Report' : ''}
                                {isStart == true && isPageLoaded == false ? 'Generating Report Please Wait...' : ''}
                                {isStart == false && isPageLoaded == true ? 'Report Generated' : ''}
                            </button>
                            <button className='btns' style={{
                                backgroundColor: '#0d0646',
                                padding: '13px',
                                borderColor: 'darkblue',
                                color: '#e0e0f5',
                                // fontSize: 'initial',
                                fontWeight: '700',
                                borderRadius: '30px'
                                // float:'right'

                            }}
                                onClick={() => onBtnExport()}>
                                Download Report
        </button>
                        </div>
                    </Grid>
                </Grid>
            </Grid>
            <div
                id="myGrid"
                style={{
                    height: '70vh',
                    width: '100%',
                }}
                className="ag-theme-alpine"
            >
                <AgGridReact
                    columnDefs={colDef}
                    masterDetail={true}
                    // overlayLoadingTemplate={overlayLoadingTemplate}
                    // overlayNoRowsTemplate={overlayNoRowsTemplate}
                    defaultColDef={defaultColDef}
                    pagination={true}
                    onGridReady={onGridReady}
                    rowData={rowDataL1}
                />
            </div>
            {/* <div style={{ display: 'flex', textAlign: 'center', alignItems: 'center', width: '100%', justifyContent: 'center' }}>
                <button
                    disabled={(page === 1) ? true : false}
                    onClick={handlePrev}
                >
                    prev
        </button>
                <button
                    onClick={handleNext}
                >
                    next
        </button>
            </div> */}
            {/* {isStart == true && isPageLoaded == false ?
        <Backdrop className={classes.backdrop} open>
          <CircularProgress color="inherit" />
          <p>Loading..</p>
        </Backdrop>
        : ''}
         {isProductLoaded == false ?
        <Backdrop className={classes.backdrop} open>
          <CircularProgress color="inherit" />
          <p>Loading..</p>
        </Backdrop>
        : ''} */}
        </div>
    );
};

export default Discount_Report;

var filterParams = {
    comparator: function (filterLocalDateAtMidnight, cellValue) {
        var dateAsString = cellValue;
        if (dateAsString == null) return -1;
        var dateParts = dateAsString.split('/');
        var cellDate = new Date(
            Number(dateParts[2]),
            Number(dateParts[1]) - 1,
            Number(dateParts[0])
        );
        if (filterLocalDateAtMidnight.getTime() === cellDate.getTime()) {
            return 0;
        }
        if (cellDate < filterLocalDateAtMidnight) {
            return -1;
        }
        if (cellDate > filterLocalDateAtMidnight) {
            return 1;
        }
    },
    browserDatePicker: true,
    minValidYear: 2000,
    maxValidYear: 2021,
};