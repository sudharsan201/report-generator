import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { withRouter } from 'react-router-dom';
import { withStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import AccountCircle from "@material-ui/icons/AccountCircle";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import PersonOutlineOutlinedIcon from '@material-ui/icons/PersonOutlineOutlined';
import LocalOfferIcon from '@mui/icons-material/LocalOffer';
// import AssignmentTurnedInOutlinedIcon from '@material-ui/icons/AssignmentTurnedInOutlined';
// import LanguageOutlinedIcon from '@material-ui/icons/LanguageOutlined';
import HomeOutlinedIcon from '@material-ui/icons/HomeOutlined';
import SupervisorAccountOutlinedIcon from '@material-ui/icons/SupervisorAccountOutlined';
import Discount_Report from './discount_report';
import Total_data from './dimensions_report';


const drawerWidth = 220;


const styles = theme => ({
  root: {
    display: "flex", 
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36
  },
  menuButtonIconClosed: {
    transition: theme.transitions.create(["transform"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    transform: "rotate(0deg)"
  },
  menuButtonIconOpen: {
    transition: theme.transitions.create(["transform"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    transform: "rotate(180deg)"
  },
  hide: {
    display: "none"
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: "nowrap"
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  drawerClose: {
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    overflowX: "hidden",
    width: theme.spacing.unit * 7 + 1,
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing.unit * 9 + 1
    }
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    marginTop: theme.spacing.unit,
    justifyContent: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3
  },
  grow: {
    flexGrow: 1
  }
});
// const { history } = useHistory;

class AdminNavbar extends React.Component {
  state = {
    open: false,
    anchorEl: null,
    home:false,
    discount:false,
    dimensions:false,
  };

  handleDrawerOpen = () => {
    this.setState({ open: !this.state.open });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };

  handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget });
   
  };
  handleClose = () => {
    this.setState({ anchorEl: null });
  };
  
    handleDiscount = () => {
    this.setState({discount: true });
    this.setState({dimensions:false});
    this.setState({ home: false });
     <>
    <Typography paragraph variant='h4'>
      Admin Page
    </Typography>
    <Typography paragraph>
    This is  Report Generator
    </Typography>
    </>
    // this.setState({home:false});
    // this.setState({role:false});
  };

  handleHome = () => {
    this.setState({ home: true });
    this.setState({discount:false});
    this.setState({dimensions:false});
  };

  handleDimensions = () => {
    this.setState({dimensions: true });
    this.setState({discount:false});
    this.setState({home:false});
  };
  handleLogin = (param) => {
    localStorage.removeItem('User');
    // localStorage.removeItem('Role');
    this.props.history.push('/', 1000);
  }

  render() {
    const { match, location, history } = this.props;
    const { classes, theme } = this.props;
    const { anchorEl } = this.state;
    const open = Boolean(anchorEl);
    
    return (
      <div className={classes.root}>
        <CssBaseline />
        <AppBar
          position="fixed"
          className={classes.appBar}
          fooJon={classNames(classes.appBar, {
            [classes.appBarShift]: this.state.open
          })}
        >
          <Toolbar disableGutters={true}>
            <IconButton
              style={{color:"#fff",fontWeight:"bold"}}
              aria-label="Open drawer"
              onClick={this.handleDrawerOpen}
              className={classes.menuButton}
            >
              <MenuIcon
                classes={{
                  root: this.state.open
                    ? classes.menuButtonIconOpen
                    : classes.menuButtonIconClosed
                }}
              />
            </IconButton>
            <Typography
              variant="h6"
              style={{color:"#fff",fontWeight:"bold"}}
              className={classes.grow}
              noWrap
            >
              Report Generator
            </Typography>
            <div>
              <IconButton
                aria-owns={open ? "menu-appbar" : undefined}
                aria-haspopup="true"
                style={{color:"#fff",fontWeight:"bold"}}
                onClick={this.handleMenu}
                color="inherit"
              >
                <AccountCircle />
              </IconButton>
              <Menu
                id="menu-appbar"
                style={{color:"#fff",fontWeight:"bold"}}
                anchorEl={anchorEl}
                anchorOrigin={{
                  vertical: "top",
                  horizontal: "right"
                }}
                transformOrigin={{
                  vertical: "top",
                  horizontal: "right"
                }}
                open={open}
                onClose={this.handleClose}
              >
                {/* <MenuItem onClick={this.handleClose}>Profile</MenuItem>
                <MenuItem onClick={this.handleClose}>My account</MenuItem> */}
                <MenuItem onClick={this.handleLogin }>Logout</MenuItem>
              </Menu>
            </div>
          </Toolbar>
        </AppBar>
        <Drawer
          variant="permanent"
          className={classNames(classes.drawer, {
            [classes.drawerOpen]: this.state.open,
            [classes.drawerClose]: !this.state.open
          })}
          classes={{
            paper: classNames({
              [classes.drawerOpen]: this.state.open,
              [classes.drawerClose]: !this.state.open
            })
          }}
          open={this.state.open}
        >
          <div className={classes.toolbar} />
          <List>
          <ListItem
               button
               onClick={this.handleHome}
               >
                <ListItemIcon>
                    <HomeOutlinedIcon/> 
                </ListItemIcon>
                Home
                <ListItemText />
              </ListItem>

              <ListItem
               button
               onClick={this.handleDiscount}
               >
                <ListItemIcon>
                    {/* <PersonOutlineOutlinedIcon/>  */}
                    <LocalOfferIcon/>
                </ListItemIcon>
                Discount Report
                <ListItemText />
              </ListItem>

              <ListItem
               button
               onClick={this.handleDimensions}
               >
                <ListItemIcon>
                    <SupervisorAccountOutlinedIcon/>
                </ListItemIcon>
                Dimensions Report
                <ListItemText />
              </ListItem>
              
          </List>
        </Drawer>
        <main className={classes.content}>
          <div className={classes.toolbar} />
          { !(this.state.discount || this.state.dimensions)?
          <>
          <Typography paragraph variant='h4'>
            Admin Page
          </Typography>
          <Typography paragraph variant='h6' >
            Sales Report
          </Typography>
          </>:
          <>
          {this.state.home?
            <>
            <Typography paragraph variant='h4'>
              Admin Page
            </Typography>
            <Typography paragraph>
            This is Report Generator
            </Typography>
            </>:''
          }
          {this.state.discount?
          <>
          <Typography paragraph variant='h5'>
              Discount Sales Report
            </Typography>
            <Discount_Report/>
            </>:''
          }
           {this.state.dimensions?
           <>
           <Typography paragraph variant='h5'>
               Dimensions Sales Report
             </Typography>
             <Total_data/>
             </>:''
          }
          </>
          }
        </main>
      </div>
    );
  }
}

AdminNavbar.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
};

const AdminNavbars = withRouter(AdminNavbar);

export default withStyles(styles, { withTheme: true })(AdminNavbars);