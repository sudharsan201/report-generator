import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import { TextField, Typography, Button, IconButton } from '@material-ui/core';
import AccountCircle from '@material-ui/icons/AccountCircle';
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import axios from 'axios';

import { FaUserAlt, FaLock } from 'react-icons/fa';
import { AiOutlineDoubleRight } from 'react-icons/ai';
import { message } from 'antd';
import './style.css';
import 'antd/dist/antd.css';
import './../component/design.css';


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,

       
    },
    main: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    image: {


    },
    label: {
        width: '140% !important',
    },
    logo: {
        marginTop: '6%',

    },
    head: {
        // backgroundImage:`url(${Bg})`,
        backgroundRepeat: 'no-repeat',
        position: 'relative',
        backgroundPosition: 'center',
        width: 'unset !important',
        margin: '0 !important',
    },
    name: {
        marginBottom: '12px',
        marginTop: '15px',


    },


    margin: {
        margin: theme.spacing(1),
    },
}));

export default function LoginPage() {
    const history = useHistory();
    const classes = useStyles();
    const [username, setUsername] = useState();
    const [password, setPassword] = useState();
    const [showPassword, setShowPassword] = useState(false);
    const handleClickShowPassword = () => setShowPassword(!showPassword);
    const handleMouseDownPassword = () => setShowPassword(!showPassword);




    function handleLoginRequest() {
        if (!username) {
            message.warning('Enter Username');
        }
        else if (!password) {
            message.warning('Enter Password');
        }
        if (username && password) {
            const data = {
                username: username,
                password: password,
            }

            axios.post(`https://mysparklebox.com/wp-json/custom-plugin/login`, data).then((response) => {
                console.log("response:post", response)
                if (response.data.status === 200) {
                    localStorage.setItem('User', true)
                   
                    message.success({
                        content: 'Login Successfully ',
                        className: 'custom-class',
                        style: {
                            marginTop: '20vh',
                        },
                    });
                    history.push('/AdminNavbar');
                    console.log('User', response.data.user_id);
                    setUsername('');
                    setPassword('');

                }
                else {
                    message.error({
                        content: 'Invalid Credentials',
                        className: 'custom-class',
                        style: {
                            marginTop: '20vh',
                        },
                    });
                }
                return response

            })
        }

    }






    return (
        <div className='Login'>
             <div class="container" >
                <div class="screen">
                    <div class="screen__content">
                        <form class="login">
                            <div class="login__field">
                                <FaUserAlt className='login__icon' />

                                <input type="text" class="login__input" placeholder="User name"
                                    onChange={(e) => setUsername(e.target.value)}
                                />
                            </div>
                            <div class="login__field">
                                <FaLock className='login__icon' />
                                <input type={showPassword ? "text" : "password"}
                                    class="login__input" placeholder="Password" onChange={(e) => setPassword(e.target.value)}
                                />
                            </div>
                            <Grid item xs={12} md={12} lg={12} className={classes.name}>
                            <Button class="button login__submit"
                                variant="contained"
                                color="primary"
                                size="large"
                                onClick={handleLoginRequest}
                                
                            >
                                <span class="button__text">Log In N </span>
                                <AiOutlineDoubleRight className="button__icon" />
                            </Button>

                        </Grid>
                             {/* <button class="button login__submit" onClick={handleLoginRequest}>
                                <span class="button__text">Log In Now</span>
                                <AiOutlineDoubleRight className="button__icon" />
                              </button>  */}
                        </form>

                    </div>
                    <div class="screen__background">
                        <span class="screen__background__shape screen__background__shape4"></span>
                        <span class="screen__background__shape screen__background__shape3"></span>
                        <span class="screen__background__shape screen__background__shape2"></span>
                        <span class="screen__background__shape screen__background__shape1"></span>
                    </div>
                </div>
            </div>
            {/* <Grid spacing={2} className={classes.head}>
                <Grid container item xs={12} md={12} lg={12}>
                </Grid>
                <Grid item xs={12} md={12} lg={12} className={classes.main}>
                    <Grid item xs={12} md={6} lg={6} className={classes.margin}>
                        <Grid item xs={6} md={6} lg={6} className={classes.name}>
                            <Typography variant="h4">Login</Typography>
                        </Grid>
                        <Grid item xs={10} md={10} lg={4} className={classes.name}>
                            <TextField className={classes.label}
                                id="input-with-icon-grid"
                                label="Username"
                                variant='outlined'
                                onChange={(e) => setUsername(e.target.value)}
                                InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                            <AccountCircle />
                                        </InputAdornment>
                                    ),
                                }}
                            />
                        </Grid>
                        <Grid item xs={10} md={10} lg={4} className={classes.name}>
                            <TextField className={classes.label}
                                id="input-with-icon-grid"
                                label='Password'
                                variant="outlined"
                                type={showPassword ? "text" : "password"}
                                onChange={(e) => setPassword(e.target.value)}
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position="start">
                                            <IconButton
                                                aria-label="toggle password visibility"
                                                onClick={handleClickShowPassword}
                                                onMouseDown={handleMouseDownPassword}
                                            >
                                                {showPassword ? <Visibility /> : <VisibilityOff />}
                                            </IconButton>
                                        </InputAdornment>
                                    )
                                }}
                            />

                        </Grid>
                        <Grid item xs={10} md={10} lg={6} className={classes.name}>
                            <Button
                                variant="contained"
                                color="primary"
                                size="large"
                                onClick={handleLoginRequest}
                                style={{ color: "#fff", fontWeight: "bold" }}
                            >
                                Login
                            </Button>

                        </Grid>
                    </Grid>
                </Grid>
            </Grid> */}

        </div>
    );
}