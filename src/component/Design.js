// import React from 'react';
import React, { useEffect, useState } from 'react';
import './design.css';
import { FaUserAlt, FaLock } from 'react-icons/fa';
import { AiOutlineDoubleRight } from 'react-icons/ai';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import { message } from 'antd';
import './style.css';
import 'antd/dist/antd.css';



const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
       




    },
    main: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    image: {


    },
    label: {
        width: '140% !important',
    },
    logo: {
        marginTop: '6%',

    },
    head: {
        // backgroundImage:`url(${Bg})`,
        backgroundRepeat: 'no-repeat',
        position: 'relative',
        backgroundPosition: 'center',
        width: 'unset !important',
        margin: '0 !important',
    },
    name: {
        marginBottom: '12px',
        marginTop: '15px',


    },


    margin: {
        margin: theme.spacing(1),
    },
}));





const Design = () => {

    const history = useHistory();
    const classes = useStyles();
    const [username, setUsername] = useState();
    const [password, setPassword] = useState();
    const [showPassword, setShowPassword] = useState(false);
    const handleClickShowPassword = () => setShowPassword(!showPassword);
    const handleMouseDownPassword = () => setShowPassword(!showPassword);




    function handleLoginRequest() {
        console.log("maneesha")
        if (!username) {
            message.warning('Enter Username');
        }
        else if (!password) {
            message.warning('Enter Password');
        }
        if (username && password) {
            const data = {
                username: username,
                password: password,
            }
            // console.log('machingdata', data)

            axios.post(`https://mysparklebox.com/wp-json/custom-plugin/login`, data).then((response) => {
                console.log("response:post", response)
                if (response.data.status === 200) {
                    localStorage.setItem('User', true)
                    history.push('/AdminNavbar');
                    console.log('User', response.data.user_id);
                    setUsername('');
                    setPassword('');
                    message.success({
                        content: 'Login Successfully ',
                        className: 'custom-class',
                        style: {
                            marginTop: '20vh',
                        },
                    });

                }
                else {
                    message.error({
                        content: 'Invalid Credentials',
                        className: 'custom-class',
                        style: {
                            marginTop: '20vh',
                        },
                    });
                }
                return response

            })
        }

    }




    return (

        <div>
            <div class="container">
                <div class="screen">
                    <div class="screen__content">
                        <form class="login">
                            <div class="login__field">
                                <FaUserAlt className='login__icon' />

                                <input type="text" class="login__input" placeholder="User name"
                                    onChange={(e) => setUsername(e.target.value)}
                                />
                            </div>
                            <div class="login__field">
                                <FaLock className='login__icon' />
                                <input type={showPassword ? "text" : "password"}
                                    class="login__input" placeholder="Password" onChange={(e) => setPassword(e.target.value)}
                                />
                            </div>
                            <button class="button login__submit" onClick={handleLoginRequest}>
                                <span class="button__text">Log In Now</span>
                                <AiOutlineDoubleRight className="button__icon" />
                            </button>
                        </form>

                    </div>
                    <div class="screen__background">
                        <span class="screen__background__shape screen__background__shape4"></span>
                        <span class="screen__background__shape screen__background__shape3"></span>
                        <span class="screen__background__shape screen__background__shape2"></span>
                        <span class="screen__background__shape screen__background__shape1"></span>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default Design;