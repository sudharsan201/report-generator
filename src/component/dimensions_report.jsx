import React, { useState, useEffect, useRef } from 'react';
import { AgGridColumn, AgGridReact } from 'ag-grid-react';
import moment from "moment";
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import axios from 'axios';
// import Backdrop from '@material-ui/core/Backdrop';
// import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';



const useStyles = makeStyles((theme) => ({
  root: {
    // flexGrow: 1,



  },
  head: {
    display: 'flex',
    textAlign: 'center',
    alignItems: 'center',
    width: '100%',
    justifyContent: 'center',
    columnGap: '20px'

  },
  main: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',

  },
  "@media (min-width: 300px ) and (max-width:1100px)": {
    main: {
      display: 'block',
    },
    btns: {
      outline: "unset",
    },
    btn: {

      margin: '10px auto',

    }
  },
  btn: {
    display: 'flex',

  },


  dt1: {
    marginRight: '10px',

  },



  page: {
    '& > *': {
      marginTop: theme.spacing(2),
    },
  },

  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
  downloadbutton: {
    display: "block",
  }
}));




const Total_data = () => {
  const classes = useStyles();
  const [rowData, setRowData] = useState([]);
  let [product, setProduct] = useState([]);
  const [number, setNumber] = useState(1000);
  const [startDate, setStartDate] = useState(new Date().getFullYear() + '-' + ("0" + (new Date().getMonth() + 1)).slice(-2) + '-' + ("0" + new Date().getDate()).slice(-2));
  const [endDate, setEnddate] = useState(new Date().getFullYear() + '-' + ("0" + (new Date().getMonth() + 1)).slice(-2) + '-' + ("0" + new Date().getDate()).slice(-2))
  const [page, setPage] = useState(1);
  const [fullData, setFullData] = useState([]);
  const [order, setOrder] = useState([]);
  const [allOrderData, setallOrderData] = useState([]);
  let [finalOrderData, setfinalOrderData] = useState([]);
  const [isPageLoaded, setisPageLoaded] = useState(false);
  const [isStart, setisStart] = useState(false);
  const [isProductLoaded, setisProductLoaded] = useState(false);
  let [rowDataL, setrowDataL] = useState([]);
  let [rowDataL1, setrowDataL1] = useState([]);


  useEffect(() => {
    const headers = {
      'Content-Type': 'application/json',
      Authorization: 'Basic Y2tfYmVkZDVkOGQ3MjczMGEyNWM2NDk4NGJjY2JlOTZhOGZiOGUwZDhmNzpjc181MjVlMmU5MDY3YWVmYThjN2U2MDViZTIwMTY5Nzc4OTdlODM4ZTVj',
    }
    let orders = []
    axios.get(`https://mysparklebox.com/wp-json/wc/v2/orders/?after=` + startDate + `T00:00:00&before=` + endDate + `T23:59:59&per_page=${100}&page=${page}`, { headers })
      .then(data => {
        console.log(data.headers);
        if (data?.data?.length > 0) {
          setRowData(data?.data);
        }
      }
      );
  }, [page, startDate, endDate, product]);

  function getData(loop) {
    //setisPageLoaded(false);
    //for(var i=1;i<=loop;i++){
    const headers = {
      'Content-Type': 'application/json',
      Authorization: 'Basic Y2tfYmVkZDVkOGQ3MjczMGEyNWM2NDk4NGJjY2JlOTZhOGZiOGUwZDhmNzpjc181MjVlMmU5MDY3YWVmYThjN2U2MDViZTIwMTY5Nzc4OTdlODM4ZTVj',
    }

    axios.get(`https://mysparklebox.com/wp-json/wc/v2/orders/?after=` + startDate + `T00:00:00&before=` + endDate + `T23:59:59&per_page=${100}&page=${loop}`, { headers })
      // .then(response => response.json())
      .then(data => {
        console.log(data.data);
        allOrderData.push(data.data);

        if (loop == 1) {
          setisPageLoaded(false);
          setisStart(false);

          var newArrayOrder = allOrderData.flat(Infinity);
          console.log(newArrayOrder);
          //allOrderData=[...newArrayOrder];
          // getProduct();
          setallOrderData(allOrderData.flat(Infinity));
          finalOrderData = allOrderData.flat(Infinity);
          setfinalOrderData(finalOrderData);
          //setRowData(newArrayOrder);
          //console.log("ROW DATA");
          console.log(finalOrderData);

        }
        else {
          getData(loop - 1);
        }
      }
      );
    // }
  }

  function getProduct(pg) {
    const headers = {
      'Content-Type': 'application/json',
      Authorization: 'Basic Y2tfYmVkZDVkOGQ3MjczMGEyNWM2NDk4NGJjY2JlOTZhOGZiOGUwZDhmNzpjc181MjVlMmU5MDY3YWVmYThjN2U2MDViZTIwMTY5Nzc4OTdlODM4ZTVj',
    }
    let products = []
    axios.get(`https://mysparklebox.com/wp-json/wc/v3/products/?per_page=100&page=${pg}`, { headers })
      .then(data => {
        if (data?.data?.length > 0) {
          product.push(data.data);
          product = product.flat(Infinity);
          if (pg == 1) {
            setProduct(product);
            console.log("product")
            console.log(product);
          }
          else {
            getProduct(pg - 1);
          }

        }
      }
      );
  }
  useEffect(() => {
    if (isPageLoaded) {
      console.log(allOrderData);
    }
  }, [isPageLoaded, allOrderData]);
  let product_id;
  console.log('rowData', rowData)


  useEffect(() => {
    const headers = {
      'Content-Type': 'application/json',
      Authorization: 'Basic Y2tfYmVkZDVkOGQ3MjczMGEyNWM2NDk4NGJjY2JlOTZhOGZiOGUwZDhmNzpjc181MjVlMmU5MDY3YWVmYThjN2U2MDViZTIwMTY5Nzc4OTdlODM4ZTVj',
    }
    let products = []
    axios.get(`https://mysparklebox.com/wp-json/wc/v3/products/?per_page=100&page=1`, { headers })
      .then(data => {
        if (data?.data?.length > 0) {
          //setProduct(data?.data);
          getProduct(parseInt(data.headers['x-wp-totalpages']));
        }
      }
      );
  }, []);

  let productId, productName, quantity, skuCode, unitsSold, dheight, dwidth, dlength, dweight;
  // let sNo;
  console.log("product", product)


  useEffect(() => {
    if (finalOrderData.length > 0 && product.length > 0) {

      setRowData([])
      setrowDataL([])
      setrowDataL1([])
      finalOrderData?.map((n, i) => {
        unitsSold = n.line_items
        unitsSold.map((prod, index) => {
          productName = prod.name;
          quantity = prod.quantity;
          productId = prod.product_id;
          skuCode = prod?.sku;
          let total = 0;
          let partialTotal = 0;
          let payment_method = "";
          let payment_method1 = "";
          let payment_method2 = "";
          let dispatchDate = "";
          let skuName = '';
          let buyergst = '';
          n?.meta_data?.map((partial) => {
            if (partial?.key === "mwb_woo_pcod_saved_partial_amount") {
              partialTotal = parseFloat(n?.total) - 99;
              payment_method = "Partial COD";
            }
            if (n?.payment_method_title === "Pay with Razorpay" || n?.payment_method_title === "Sezzle" || n?.payment_method_title === "Zest") {
              payment_method2 = "Pre-Paid";
              total = parseFloat(n?.total);
            }
            if (partial?.key === "dispatch_date") {
              dispatchDate = partial?.value;
            }
            else {
              payment_method1 = 'COD';
              total = parseFloat(n?.total);
            }

            if (partial?.key === "_billing_buyer_gst") {
              buyergst = partial?.value;
            }
          })
          let date = moment(n.date_created).format("L");
          let datel = moment(dispatchDate).format("L");



          product?.map((p) => {
            if (p.id === productId) {
              // console.log('productId1', productId);
              dheight = p.dimensions?.height;
              dwidth = p.dimensions?.width;
              dlength = p.dimensions?.length;
              dweight = p.weight;
              p?.attributes?.map((a) => {
                if (a.name === "SKU Name") {
                  skuName = a?.options
                }
              })
              if (n.status != 'cancel-request'
                && n.status != 'cancelled'
                && n.status != 'failed'
              ) {
                rowDataL.push({
                  // 'sno': i,
                  'date_created': date,
                  'id': n.id,
                  'cart_tax': n.cart_tax,
                  'status': n.status,
                  // 'pId': productId,
                  'ProductName': productName,
                  'SkuCode': skuCode,
                  'SkuName': skuName,
                  'quantity': quantity,
                  'quntity': n.quntity,
                  'Dead Weight': productName,
                  'billing': n.billing,
                  'shipping': n.shipping,
                  'payment_method_title': payment_method ? payment_method : payment_method2 ? payment_method2 : payment_method1,
                  'channel': 'woocommerce',
                  'skuName': skuName,
                  'dheight': dheight,
                  'dwidth': dwidth,
                  'dlength': dlength,
                  'dweight': dweight,
                  'total': partialTotal ? partialTotal : total,
                  'dispatchdate': datel,
                  // 'buyergst': buyergst,
                })
              }
            }
          })
        })
      })
      setisProductLoaded(false);
      setrowDataL1(rowDataL);

    }

  }, [finalOrderData, product, rowDataL, rowDataL1])


  const [gridApi, setGridApi] = useState([]);
  const colDef = [
    // { headerName: 'S.No', field: 'sno', showRowGroup: true, minWidth: 120, },
    { headerName: 'Order Date', field: 'date_created', showRowGroup: true, minWidth: 220, },
    { headerName: 'Order ID', field: 'id', showRowGroup: true, minWidth: 220, },
    { headerName: 'Status', field: 'status', showRowGroup: true, minWidth: 220, },
    { headerName: 'Product Name', field: 'ProductName', showRowGroup: true, minWidth: 320, },
    // { headerName: 'PId ', field: 'pId', minWidth: 220, },
    { headerName: 'SKU Code', field: 'SkuCode', showRowGroup: true, minWidth: 320, },
    { headerName: 'SKU Name', field: 'SkuName', showRowGroup: true, minWidth: 320, },
    { headerName: 'Units Sold', field: 'quantity', showRowGroup: true, minWidth: 220, },
    { headerName: 'Weight', field: 'dweight', showRowGroup: true, minWidth: 220, },
    { headerName: 'Height', field: 'dheight', showRowGroup: true, minWidth: 220, },
    { headerName: 'Width', field: 'dwidth', showRowGroup: true, minWidth: 220, },
    { headerName: 'Length', field: 'dlength', showRowGroup: true, minWidth: 220, },
    { headerName: 'Payment Gateway', field: 'payment_method_title', showRowGroup: true, minWidth: 220, },
    { headerName: 'Dispatch Date', field: 'dispatchdate', showRowGroup: true, minWidth: 220, },
    // { headerName: 'Buyer GST', field: 'buyergst', showRowGroup: true, minWidth: 220, },
    { headerName: 'Channel', field: 'channel', showRowGroup: true, showRowGroup: true, minWidth: 220, },
    { headerName: 'Total', field: 'total', showRowGroup: true, minWidth: 220, },
    { headerName: 'Customer Name', field: 'billing.first_name', showRowGroup: true, minWidth: 220, },
    { headerName: 'Phone', field: 'billing.phone', showRowGroup: true, minWidth: 220, },
    { headerName: 'Email', field: 'billing.email', showRowGroup: true, minWidth: 220, },
    { headerName: ' Billing Address', field: 'billing.address_1', showRowGroup: true, minWidth: 220, },
    { headerName: 'Shipping Address', field: 'shipping.address_1', showRowGroup: true, minWidth: 220, },
    { headerName: 'Pincode', field: 'billing.postcode', showRowGroup: true, minWidth: 220, },

  ]
  const defaultColDef = {
    flex: 1,
    minWidth: 150,
    filter: true,

    resizable: true,
  };



  const onGridReady = (params) => {

    setGridApi(params);
  };
  const onBtnExport = () => {

    gridApi.api.exportDataAsCsv();

  };

  const onGenReport = () => {
    setisStart(true);
    setisPageLoaded(false);
    setisProductLoaded(true);

    const headers = {
      'Content-Type': 'application/json',
      Authorization: 'Basic Y2tfYmVkZDVkOGQ3MjczMGEyNWM2NDk4NGJjY2JlOTZhOGZiOGUwZDhmNzpjc181MjVlMmU5MDY3YWVmYThjN2U2MDViZTIwMTY5Nzc4OTdlODM4ZTVj',
    }
    let orders = []
    axios.get(`https://mysparklebox.com/wp-json/wc/v2/orders/?after=` + startDate + `T00:00:00&before=` + endDate + `T23:59:59&per_page=${100}&page=${page}`, { headers })
      // .then(response => response.json())
      .then(data => {
        console.log(data.headers);
        allOrderData.length = 0;
        getData(parseInt(data.headers['x-wp-totalpages']));
        if (data?.data?.length > 0) {
          setRowData(data?.data);
        }
      }
      );
  };

  // function handlePrev() {
  //   if (page > 1) {
  //     setPage(page - 1);
  //   }
  // }
  // function handleNext() {
  //   setPage(page + 1);
  // }
  console.log('fullData', fullData);
  console.log('order', order);
  console.log('rowdata', rowDataL);

  return (
    <div className={classes.root}>
      <Grid spacing={2} className={classes.head}>
        <Grid container item xs={12} md={12} lg={12}>
          <Grid item xs={12} md={12} lg={12} className={classes.main}>
            <div >{isProductLoaded ? <p className='dt1' style={{ marginRight: '10px', padding: '10px', background: '#f55e5e', color: 'white', borderRadius: '10px' }}>Loading Orders Please wait...</p> : ''}</div>
            <br />
            <div className='dt1'> <label style={{ marginRight: '10px', fontSize: 'large' }}>Start Date:</label>
              <input
                style={{ marginRight: '10px', borderRadius: '30px', padding: '10px', color: 'darkblue' }}
                type='date'
                value={startDate}
                onChange={(e) => setStartDate(e.target.value)}
              />
            </div>
            <div className='dt1'>
              <label style={{ marginRight: '10px', fontSize: 'large' }}>End Date:</label>
              <input style={{ marginRight: '10px', borderRadius: '30px', padding: '10px', color: 'darkblue' }}
                type='date'
                value={endDate}
                onChange={(e) => setEnddate(e.target.value)}
              />
            </div>

            <div className='btn'>
              <button className='btns' style={{
                marginRight: '10px',
                backgroundColor: '#0d0646',
                padding: '13px',
                borderColor: 'darkblue',
                color: '#e0e0f5',
                // fontSize: 'initial',
                fontWeight: '700',
                borderRadius: '30px'
                // float:'right'

              }}
                onClick={() => onGenReport()}>
                {isStart == false && isPageLoaded == false ? 'Generate Report' : ''}
                {isStart == true && isPageLoaded == false ? 'Generating Report Please Wait...' : ''}
                {isStart == false && isPageLoaded == true ? 'Report Generated' : ''}
              </button>
              <button className='btns' style={{
                backgroundColor: '#0d0646',
                padding: '13px',
                borderColor: 'darkblue',
                color: '#e0e0f5',
                // fontSize: 'initial',
                fontWeight: '700',
                borderRadius: '30px'
                // float:'right'

              }}
                onClick={() => onBtnExport()}>
                Download Report
        </button>

            </div>

          </Grid>

        </Grid>

      </Grid>



      <div
        id="myGrid"
        style={{
          height: '70vh',
          width: '100%',
          enableCellTextSelection: "true",
        }}
        className="ag-theme-alpine"
      >
        <AgGridReact
          columnDefs={colDef}
          enableCellTextSelection={true}

          masterDetail={true}
          // overlayLoadingTemplate={overlayLoadingTemplate}
          // overlayNoRowsTemplate={overlayNoRowsTemplate}
          defaultColDef={defaultColDef}
          pagination={true}
          onGridReady={onGridReady}
          rowData={rowDataL1}
        />
      </div>
      {/* <div style={{ display: 'flex', textAlign: 'center', alignItems: 'center', width: '100%', justifyContent: 'center' }}>
        <button
          disabled={(page === 1) ? true : false}
          onClick={handlePrev}
        >
          prev
        </button>
        <button
          onClick={handleNext}
        >
          next
        </button>
      </div> */}
      {/* {isStart == true && isPageLoaded == false ?
        <Backdrop className={classes.backdrop} open>
          <CircularProgress color="inherit" />
          <p>Loading..</p>
        </Backdrop>
        : ''}
         {isProductLoaded == false ?
        <Backdrop className={classes.backdrop} open>
          <CircularProgress color="inherit" />
          <p>Loading..</p>
        </Backdrop>
        : ''} */}
    </div>
  );
};

export default Total_data;
var filterParams = {
  comparator: function (filterLocalDateAtMidnight, cellValue) {
    var dateAsString = cellValue;
    if (dateAsString == null) return -1;
    var dateParts = dateAsString.split('/');
    var cellDate = new Date(
      Number(dateParts[2]),
      Number(dateParts[1]) - 1,
      Number(dateParts[0])
    );
    if (filterLocalDateAtMidnight.getTime() === cellDate.getTime()) {
      return 0;
    }
    if (cellDate < filterLocalDateAtMidnight) {
      return -1;
    }
    if (cellDate > filterLocalDateAtMidnight) {
      return 1;
    }
  },
  browserDatePicker: true,
  minValidYear: 2000,
  maxValidYear: 2021,
};