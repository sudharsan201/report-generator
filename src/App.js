import './App.css';
import Discount_Report from './component/discount_report';
import Total_data from './component/dimensions_report';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import LoginPage from './component/Login';
import AdminNavbar from './component/navbar';
import PrivateRoute from './Privateroute';



function App() {
  const isAuthenticated = localStorage.getItem('User');
  return (
    <div className="App">
      {/* <Design/> */}
      <Router>
        <Switch>
          <PrivateRoute
            exact path='/dimensions'
            component={Total_data} 
            />
          <PrivateRoute
            exact path='/Discount_Report'
            component={Discount_Report} />
         
          <PrivateRoute  exact path='/AdminNavbar' component={AdminNavbar} />
            
          <Route exact path='/' component={LoginPage}></Route>

        </Switch>
      </Router>
    </div>
  );
}

export default App;
